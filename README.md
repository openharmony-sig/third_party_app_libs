<h1 align="center">OpenHarmony下可以直接使用的三方组件汇总</h1> 

### 本库收集了在OpenHarmony中可以直接依赖使用的原JavaScript、TypeScript三方组件，也欢迎开发者增加三方组件提PR到列表中。
#### 1、三方组件名称：三方组件的名称
#### 2、描述：三方组件的基本功能描述
#### 3、sample地址（非必须）：样例地址

### [OpenHarmony三方组件资源汇总](https://gitee.com/openharmony-tpc/tpc_resource) : 基于OpenHarmony开发的三方组件。

## 目录

- [工具](#工具)
- [三方组件](#三方组件)
    - [文件数据](#文件数据)
        - [文件解析](#文件解析)
        - [编码解码](#编码解码)
    - [工具库](#工具库)
    - [框架](#框架)
    - [网络通信](#网络通信)
    - [其他](#其他)

## 工具

- [IDE官方下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio) - DevEco Studio

[返回目录](#目录)

## <a name="三方组件"></a>三方组件

|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|

### <a name="文件数据"></a>文件数据

#### <a name="文件解析"></a>文件解析

|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|
| [node-csv](https://github.com/adaltas/node-csv) | 可用于解析csv文件，生成csv文件 | [sample地址](https://gitee.com/openharmony-tpc/node-csv)|
| [is-png](https://github.com/sindresorhus/is-png) | is-png是一个判断图片格式的库，根据图片的文件数据，判断图片是否为png格式。| [sample地址](https://gitee.com/openharmony-sig/is-png)|   
| [is-webp](https://github.com/sindresorhus/is-webp) | is-webp是一款根据图片文件头部部分数据，判断当前图片是否是webp格式的库。|[sample地址](https://gitee.com/openharmony-sig/is-webp)| 
| [gifuct-js](https://github.com/matt-way/gifuct-js) | gifuct-js是一款GIF文件数据解码库。|
| [node-xml2js](https://github.com/Leonidas-from-XIV/node-xml2js) | XML 到 JavaScript 对象转换器。|[sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/xml2jsDemo)| 
| [jszip](https://github.com/xqdoo00o/jszip) | is-webp是一款根据图片文件头部部分数据，判断当前图片是否是webp格式的库。|[sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/jszipDemo)| 


[返回目录](#目录)

#### <a name="编码解码"></a>编码解码

|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|
| [protobufjs](https://github.com/protobufjs/protobuf.js) | 主要功能是序列化和反序列化，比xml更快，更高效，序列化后的体积也很小，受到广大开发者的喜爱。 | [sample地址](https://gitee.com/openharmony-tpc/protobuf)|
| [commons-codec](https://github.com/apache/commons-codec)  | 包含各种格式的简单编码器和解码器，例如 Base64 和 Hexadecimal。除了这些广泛使用的编码器和解码器之外，它还维护了一组语音编码实用程序。 | [sample地址](https://gitee.com/openharmony-tpc/commons-codec)|
| [cbor-js](https://github.com/paroga/cbor-js) | 以纯 JavaScript 实现的简明二进制对象表示 (CBOR) 数据格式 ( RFC 7049 )。| [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/cborjsDemo)|
| [js-base64](https://github.com/dankogai/js-base64)  | JavaScript 的 Base64 实现。 |
| [base64-js](https://github.com/beatgammit/base64-js)  | 纯 JS 中的 Base64 编码/解码。 |

[返回目录](#目录)

### <a name="工具库"></a>工具库

|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|
| [lodash](https://github.com/lodash/lodash) | 提供模块性、性能和额外功能的现代JavaScript实用程序库。| [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/lodashDemo) |
| [libphonenumber-js](https://github.com/catamphetamine/libphonenumber-js)  | 用于解析、格式化和验证国际电话号码。| [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/LibphoneNumber) | 
| [node-lru-cache](https://github.com/isaacs/node-lru-cache) | 一个删除最近最少使用算法的项目的缓存工具。|
| [jackson-js](https://github.com/elastos/jackson-js)  |  JavaScript 对象序列化和反序列化库。它还支持高级对象概念，例如多态性、对象标识和循环对象。|
| [commonmark](https://github.com/commonmark/commonmark.js) | 将Markdown格式文件转换为Html或者xml文件，用于在网页中显示。 | [sample地址](https://gitee.com/openharmony-tpc/commonmark) | 
| [jsdiff](https://github.com/kpdecker/jsdiff)  |  一个 JavaScript 文本差异实现。| [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/jsDiffDemo) |
| [js-joda](https://github.com/js-joda/js-joda)  |  一个 JavaScript 不可变时间日期库。|
| [buffer](https://github.com/feross/buffer) | 一个小巧的速度极快的用于协助操作二进制数据的库。 |
| [matrix](https://github.com/mljs/matrix) | 矩阵操作和计算库。 |
| [Easyrelpace](https://github.com/codsen/codsen/tree/main/packages/easy-replace) | Compare Text替换字符串。 | [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/Easyrelpace) | 
| [EventBus](https://github.com/krasimir/EventBus) | 一个主要功能是消息订阅发送的库。 | [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/EventBus) | 
| [ahocorasick](https://github.com/BrunoRB/ahocorasick) | 是字符串搜索算法的实现，能够高效的进行字符串匹配的库。 | [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/ahocorasick) | 
| [asn1](https://github.com/ComplyCloud/asn1) | 用于构建 ASN.1对象模型以及JSON 序列化/反序列化。 | [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/asn1Demo) | 
| [js-tokens](https://github.com/lydell/js-tokens) | 是一个微型JavaScript的分词器。 | [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/js-tokens) | 

[返回目录](#目录)

### <a name="框架"></a>框架

|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|
| [rxjs](https://github.com/Reactive-Extensions/RxJS) |一个通过使用可观察序列来合成异步和基于事件的程序的库。它扩展了观察者模式以支持数据/事件序列，并添加了运算符，允许您以声明方式将序列组合在一起。| [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/RxJS) | 
| [node-rules](https://github.com/mithunsatheesh/node-rules) |node-rules 是一个轻量级的正向链接规则引擎 | [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/nodeRulesDemo) |
[返回目录](#目录)

### <a name="网络通信"></a>网络通信

|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|


[返回目录](#目录)

### <a name="其他"></a>其他
|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|
| [pinyin4js](https://github.com/Kaifun/pinyin4js)  | pinyin4js是一个汉字转拼音的Javascript开源库-零依赖。 |
| [jsBinarySchemaParser](https://github.com/matt-way/jsBinarySchemaParser)  | jsBinarySchemaParser提供了uin8数据类型读取工具，并内置了gif数据块解码规则 |
| [long.js](https://github.com/dcodeIO/long.js)  | long.js是用于表示 64 位二进制补码整数值的 Long 类。 |
| [mathjs](https://github.com/josdejong/mathjs)  | 包含数字、大数、三角函数、字符串、和矩阵等数学功能。 | [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/mathjsDemo) |
| [sanitize-html](https://github.com/apostrophecms/sanitize-html)  | sanitize-html 提供了HTML清理API，支持HTML片段清理。 | [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/sanitize-html) |
| [js-joda](https://github.com/js-joda/js-joda)  | 一款不可变日期和时间开源库。 | [sample地址](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/js-joda) |

[返回目录](#目录)
